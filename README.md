![Alt text](made-with-python.svg)

```py
fields = [DBField('username', 'firstname'),
              DBField('name', 'lastname'),
              DBField('login', 'mail'),
              DBField('guitar_price', 'largeprice'),
              DBField('guitar_brand', 'brand'),
              DBField('pizza_price', 'tinyprice'),
              DBField('order_date', 'date')]

    options_path = 'Const/Const.json'

    print(SQLGenerator.generateSql_insert(options_path, "user", fields, 10))
```

## Output:

```shell
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (George, Pop, bob@neelix.com, 6242, Squier, 7, 2017-02-21 20:35:32); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (David, Morrison, innerself@synthex.com, 8959, Epiphone, 18, 2008-04-16 05:39:28); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (Angus, Cobain, unseen@dimensions.com, 4572, Gibson, 6, 2012-03-25 23:49:08); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (Paul, Berry, le@go.com, 1043, Marshall, 1, 2001-08-05 05:21:51); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (David, Harrison, unseen@dimensions.com, 2181, Epiphone, 9, 2010-12-26 23:45:09); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (David, Page, le@go.com, 6886, Squier, 18, 2010-05-28 18:36:50); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (Paul, Harrison, colors@earphonic.com, 2137, Marshall, 18, 2016-12-14 08:07:39); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (George, Page, le@go.com, 2269, Squier, 7, 2012-12-01 00:07:44); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (Jimmy, Morrison, bob@neelix.com, 9171, Fender, 4, 2021-05-15 10:08:46); 
INSERT INTO user (username, name, login, guitar_price, guitar_brand, pizza_price, order_date) VALUES (Iggy, Pop, unseen@dimensions.com, 2300, Fender, 17, 2014-02-14 17:46:07); 
```


