# -*- coding: utf-8 -*-
"""Run main to start my lovely sql generator"""

from Generator import SQLGenerator
from Generator import DBField


if __name__ == '__main__':
    fields = [DBField('username', 'firstname'),
              DBField('name', 'lastname'),
              DBField('login', 'mail'),
              DBField('guitar_price', 'largeprice'),
              DBField('guitar_brand', 'brand'),
              DBField('pizza_price', 'tinyprice'),
              DBField('order_date', 'date')]

    options_path = 'Const/Const.json'

    print(SQLGenerator.generateSql_insert(options_path, "user", fields, 10))

