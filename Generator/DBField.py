"""DBField module"""


class DBField:
    """SQLGenerator"""

    def __init__(self, name, field_type):
        self.__name = name
        self.__type = field_type

    def getName(self):
        return self.__name

    def getType(self):
        return self.__type
