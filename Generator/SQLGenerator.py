"""SQLGenerator module"""
import json
from datetime import datetime, timedelta
from random import randrange


class SQLGenerator:
    """SQLGenerator, static tool class"""

    @staticmethod
    def read_const(path):
        """
        Const.json access
        """
        with open(path, "r", encoding="utf-8") as file:
            return json.load(file)

    @staticmethod
    def random_firstname(path):
        """Return a random name from the names.json"""
        data = SQLGenerator.read_const(path)['firstnames']
        return data[randrange(len(data))]

    @staticmethod
    def random_lastname(path):
        """Return a random name from the names.json"""
        data = SQLGenerator.read_const(path)['lastnames']
        return data[randrange(len(data))]

    @staticmethod
    def random_mail(path):
        """Return a random mail from the mail.json"""
        data = SQLGenerator.read_const(path)['mails']
        return data[randrange(len(data))]

    @staticmethod
    def random_date(path):
        """Return a random date with start, end and format from the date.json"""
        # Set 3 params : start, end & format
        date_format = SQLGenerator.read_const(path)['date']['format'].strip()
        date_start = SQLGenerator.read_const(path)['date']['start'].strip()
        date_end = SQLGenerator.read_const(path)['date']['end'].strip()

        # Delta
        start = datetime.strptime(date_start, date_format)
        end = datetime.strptime(date_end, date_format)
        delta = end - start

        # Pick a random value in delta
        int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
        random_second = randrange(int_delta)
        return start + timedelta(seconds=random_second)

    @staticmethod
    def random_large_price(path):
        data = SQLGenerator.read_const(path)['large_prices']
        min_price = int(data['start'])
        max_price = int(data['end'])
        return str(randrange(min_price, max_price))

    @staticmethod
    def random_tiny_price(path):
        data = SQLGenerator.read_const(path)['tiny_prices']
        min_price = int(data['start'])
        max_price = int(data['end'])
        return str(randrange(min_price, max_price))

    @staticmethod
    def random_brand(path):
        """Return a random name from the names.json"""
        data = SQLGenerator.read_const(path)['brands']
        return data[randrange(len(data))]

    @staticmethod
    def array_to_string_comma(items):
        """
        Format
        INSERT INTO [field1, field2] VALUES [value1, value2]
        to
        INSERT INTO (field1, field2) VALUES (value1, value2)
        """
        result = ', '.join(str(item) for item in items[:-1])
        return result + ', ' + str(items[-1])

    @staticmethod
    def generateSql_insert(path, table, dbfields, count=1):
        """Main INSERT INTO SQL QUERY Generator"""
        temp_value = ''
        final_query = []
        for i in range(count):
            query1 = "INSERT INTO " + table + " ("
            query2 = ") VALUES ("
            query3 = "); \n"
            insert_fields = []
            insert_values = []
            for dbfield in dbfields:
                insert_fields.append(dbfield.getName())
                match dbfield.getType():
                    case "firstname":
                        temp_value = SQLGenerator.random_firstname(path)
                    case "lastname":
                        temp_value = SQLGenerator.random_lastname(path)
                    case "mail":
                        temp_value = SQLGenerator.random_mail(path)
                    case "date":
                        temp_value = str(SQLGenerator.random_date(path))
                    case "largeprice":
                        temp_value = SQLGenerator.random_large_price(path)
                    case "tinyprice":
                        temp_value = SQLGenerator.random_tiny_price(path)
                    case "brand":
                        temp_value = SQLGenerator.random_brand(path)
                    case _:
                        temp_value = 'null'
                insert_values.append(temp_value)

            final_query.append(query1)
            final_query.append(SQLGenerator.array_to_string_comma(insert_fields))
            final_query.append(query2)
            final_query.append(SQLGenerator.array_to_string_comma(insert_values))
            final_query.append(query3)
        return ''.join(final_query)
